# binder-notebooks

Dépôt git pour pouvoir lancer des notebooks sans installation.

Utilisation:

- Lance jupyter notebook dans un dossier:

  https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?urlpath=/tree/chemin_vers_le_dossier

# Notebook vide

Pour lancer le notebook cliquer sur ce lien: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=index.ipynb)

https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=index.ipynb

## POO

### Jeu dragonquest

Un jeu pour apprendre la programmation orientée objet en terminale.

https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=/poo/jeu-dragonquest.ipynb

## Utilisation de l'algorithme des k-plus proches voisins

### Classification d'élèves par moyennes et absences

https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=/kplusproches/classification-eleves.ipynb

### TOP-14

Un deuxième TP mis à disposition avec des données du top-14 issues de la ligue nationale de rugby:

https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=/kplusproches/classification-top-14.ipynb

## Algos sur les graphes

En lien avec le chapitre P5C2: Algorithmes sur les graphes: https://www.lyceum.fr/tg/nsi/5-algorithmique/2-algorithmes-sur-les-graphes

https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=/TP-P5C2-GraphesAlgos/Graphe-partie2.ipynb

## Données csv première

En lien avec le chapitre P4C1: Données en table: https://www.lyceum.fr/1g/nsi/4-traitement-de-donnees-en-tables/1-tables-de-donnees

https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Feduinfo%2Fbinder-notebooks/main?filepath=/TP-P4C1-csv/TP-P4C1-csv-sujet.ipynb