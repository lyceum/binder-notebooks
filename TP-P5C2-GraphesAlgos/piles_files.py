#!/usr/bin/python3
# -∗- coding: utf-8 -∗-

class Cellule:
    """une cellule d'une liste chaînée"""

    def __init__(self, v, s):
        self.valeur = v
        self.suivante = s


class Pile:
    """structure de pile"""

    def __init__(self):
        self.sommet = None
        self.taille = 0

    def est_vide(self):
        # return self.taille == 0
        return self.sommet is None

    def empiler(self, v):
        self.sommet = Cellule(v, self.sommet)
        self.taille += 1

    def depiler(self):
        if self.est_vide():
            raise IndexError("depiler sur une pile vide")
        v = self.sommet.valeur
        self.sommet = self.sommet.suivante
        self.taille -= 1
        return v

    def len(self):
        return self.taille

    def __contains__(self, value):
        '''
        Renvoie True si l'élément e est dans la pile et False  sinon.
        :param p: Une pile de type LIFO
        :param e: Un élément
        '''
        if self.sommet is None:
            return False
        elif self.sommet.valeur == value:
            return True
        else:
            P = Pile()
            P.sommet = self.sommet.suivante
            P.taille = self.taille - 1
            return P.__contains__(value)


    def affiche(self):
        '''
        Affiche la pile depuis le sommet
        :param p: Une pile de type LIFO
        '''
        if self.sommet is None:
            return 'None'
        else:
            P = Pile()
            P.sommet = self.sommet.suivante
            P.taille = self.taille - 1
            return f"{self.sommet.valeur}->{P.affiche()}"

#p=Pile()
#p.empiler(2)
#p.empiler(3)
#p.empiler(1)
#print(p.affiche())

class File():
    """structure de file"""

    def __init__(self):
        self.tete = None
        self.queue = None
        self.taille = 0

    def est_vide(self):
        # return self.taille == 0
        return self.tete is None

    def enfiler(self, x):
        c = Cellule(x, None)
        if self.est_vide():
            self.tete = c
        else:
            self.queue.suivante = c
        self.taille += 1
        self.queue = c

    def defiler(self):
        if self.est_vide():
            raise IndexError("retirer sur une file vide")
        v = self.tete.valeur
        self.tete = self.tete.suivante
        if self.tete is None:
            self.queue = None
        self.taille -= 1
        return v

    def len(self):
        return self.taille

    def __contains__(self, value):
        '''
        Renvoie True si l'élément e est dans la file et False  sinon.
        :param self: Une file de type FIFO
        :param e: Un élément
        '''
        if self.tete is None:
            return False
        elif self.tete.valeur == value:
            return True
        else:
            F = File()
            F.tete = self.tete.suivante
            F.queue = self.queue
            F.taille = self.taille - 1
            return F.__contains__(value)

    def affiche(self):
        '''
        Affiche la pile depuis le sommet
        :param p: Une pile de type LIFO
        '''
        if self.tete is None:
            return 'None'
        else:
            F = File()
            F.tete = self.tete.suivante
            F.queue = self.queue
            F.taille = self.taille - 1
            return f"{self.tete.valeur}->{F.affiche()}"


#f=File()
#f.enfiler(2)
#f.enfiler(3)
#f.enfiler(1)
#print(f.affiche())



def main(args):
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
