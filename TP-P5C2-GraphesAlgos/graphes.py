#!/usr/bin/python3
# -∗- coding: utf-8 -∗-

class GrapheMat:
    '''
    graphe représenté par une matrice d’adjacence où les sommets sont des entiers entre 0 et N-1
    '''

    def __init__(self, n):
        self.n = n
        self.adj = [[False] * n for i in range(n)]

    def ajouter_arc(self, s1, s2):
        self.adj[s1][s2] = True

    def arc(self, s1, s2):
        return self.adj[s1][s2]

    def voisins(self, s):
        v = []
        for i in range(self.n):
            if self.adj[s][i]:
                v.append(i)
        return v

    def afficher(self):
        print(self)

    def __str__(self):
        """ Affiche la matrice du graphe """ 
        #Ligne 1
        txt=" "*3
        for i in range(self.n):
            txt+=f'{i} '
        txt+="\n"
        # Deuxième ligne
        txt+=" "*3
        for i in range(self.n):
            txt+="__"
        txt+="\n"
        for i in range(self.n):
            txt+=f"{i:2d}|"
            #txt+=f"{i} -> "
            for j in range(self.n):
                if self.adj[i][j]:
                    txt+='1 '
                else:
                    txt+='0 '
            #tab = [ str(v) for v in  self.n  if  v in  self.voisins(i) else 0]
            #txt = txt+ ",".join(tab) +'\n'
            txt+='\n'
        return txt

            # for v in self.voisins(i):
            #    print(v, end=' ')
            # print()

    def degre(self, d):
        return(len(self.voisins(d)))

    def nb_arcs(self):
        nbarc = 0
        for i in range(self.n):
            nbarc += self.degre(i)
        return nbarc

    def supprimer_arc(self, s1, s2):
        try:
            self.adj[s1][s2] = False
        except:
            print(f"Attention,  il n'y a pas d'arc entre {s1} et {s2}")


class GrapheDico:
    '''    graphe représenté par un dictionnaire d’adjacence     '''

    def __init__(self):
        self.adj = {}     #dictionnaire

    def ajouter_sommet(self, s):
        if s not in self.adj:
            self.adj[s] = list()

    def ajouter_arc(self, s1, s2):
        self.ajouter_sommet(s1)
        self.ajouter_sommet(s2)
        self.adj[s1].append(s2)

    def arc(self, s1, s2):
        return s2 in self.adj[s1]

    def sommets(self):
        return list(self.adj)

    def voisins(self, sommet):
        return self.adj[sommet]
    
    def afficher(self):
        print(self)

    def __str__(self):
        txt=''
        for sommet in self.adj:
            txt+=str(sommet)+"->"+str(self.voisins(sommet))+"\n"
        return txt

    def degre(self, sommet):
        return(len(self.voisins(sommet)))

    def nb_arcs(self):
        nbarc = 0
        for i in self.adj:
            nbarc += self.degre(i)
        return nbarc

    def supprimer_arc(self, s1, s2):
        try:
            self.adj[s1].remove(s2)
        except:
            print(f"Attention,  il n'y a pas d'arc entre {s1} et {s2}")


def main(args):
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
